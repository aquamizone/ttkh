# TTKH
## Termux Teks Ke HEX
Copyleft 2019 - Iron Six  
Licensed under MIT License  
<https://mit-license.org>  
  
  
Tools ini dibuat khusus untuk pengguna Termux  

Untuk fitur baca-salin dari-dan-ke clipboard harus setup __Termux:API__ terlebih dahulu


### Instal add-on __Termux:API__
Untuk Termux yg diinstal dari Playstore <https://play.google.com/store/apps/details?id=com.termux.api>  
Untuk Termux non-Playstore <https://f-droid.org/en/packages/com.termux.api>

Setelah menginstal add-on, selanjutnya instal package "termux-api" & BASH di Termux
````
$ pkg install termux-api
$ pkg install bash
````


### Penggunaan
#### Teks ke HEX
* Teks ke HEX, baca dari input / stdin
````
$ echo "abc123" | ttkh
$ echo "abc123" | ttkh -t
$ cat /folder/file_berisi_teks.txt | ttkh
````
  * Filter output HEX tanpa spasi, gunakan argumen `-n`  
* Salin ke clipboard
````
$ echo "abc123" | ttkh -c
$ echo "abc123" | ttkh -cn
````
* Baca dari clipboard
````
$ ttkh -r
$ ttkh -rn
````
* Baca dari clipboard, lalu salin hasil proses ke clipboard
````
$ ttkh -rtc
$ ttkh -rtcn
````

#### HEX ke Teks
* HEX ke teks, baca dari input / stdin
````
$ echo "61 62 63 31 32 33" | ttkh -h
$ cat /folder/file_berisi_HEX.txt | ttkh -h
````
* Baca dari clipboard
````
$ ttkh -rh
````
* Baca dari clipboard, lalu salin hasil proses ke clipboard
````
$ ttkh -rhc
````

#### Tampilkan bantuan cara penggunaan
````
$ ttkh -b
$ ttkh --bantuan
````
  
Jika operasional clipboard hang, itu masalah dari __Termux:API__ karena masih eksperimental, versi Android juga berpengaruh  
Stop paksa dengan CTRL+C  
Kebanyakan data salinan clipboard hilang jika hang



### Instalasi
````
$ git clone https://aquamizone@bitbucket.org/aquamizone/ttkh.git
$ cd ttkh
$ ./ttkh -b
$ echo "abc123" | ./ttkh
````

+ Integrasikan ke Termux
````
$ cp ttkh /data/data/com.termux/files/usr/bin/
$ chmod 777 $(which ttkh)
$ ttkh -b
````

+ One-line installer asal sudah setup __Termux:API__
````
$ curl https://bitbucket.org/aquamizone/ttkh/raw/af3c3e42e58cb0f72858b0d9bd9c8913b4b46e20/ttkh -o /data/data/com.termux/files/usr/bin/ttkh && chmod 777 /data/data/com.termux.files/usr/bin/ttkh && ttkh -b
````

